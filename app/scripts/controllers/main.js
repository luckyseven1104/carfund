'use strict';

/**
 * @ngdoc function
 * @name carfundApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the carfundApp
 */
angular.module('carfundApp')
  .controller('MainCtrl', function ($scope, $http) {
    $scope.open_all_flag = true;
    $scope.price_slide_hide_flag = false;
    $scope.time_slide_hide_flag = false;
    $scope.car_model_hide_flag = false;
    $scope.warranty_hide_flag = false;
    $scope.car_type_hide_flag = false;
    $scope.driving_type_hide_flag = false;
    $scope.fuel_option_hide_flag = false;
    $scope.cylinders_hide_flag = false;
    $scope.transmistion_type_hide_flag = false;
    $scope.transmistion_gears_hide_flag = false;
    $scope.showDropdownCarType = false;

    $scope.car_model_count = 5;
    $scope.moreCarModel_flag = false;

    $http({
      method : 'GET',
      url : 'data/data.json'
    }).then(function (response) {
      $scope.car_model = response.data.car_model;
      $scope.warranty = response.data.warranty;
      $scope.car_type = response.data.car_type;
      $scope.driving_type = response.data.driving_type;
      $scope.fuel_option = response.data.fuel_option;
      $scope.cylinders = response.data.cylinders;
      $scope.transmistion_type = response.data.transmistion_type;
      $scope.transmistion_gears = response.data.transmistion_gears;
      $scope.car_item = response.data.car_item;
    });

    $scope.amount_slider = {
      minValue: 0,
      maxValue: 2000,
      options: {
        floor: 0,
        ceil: 2000,
        translate: function(value, sliderId, label) {
          switch (label) {
            case 'model':
              return '$' + value;
            case 'high':
              return '$' + value;
            default:
              return '$' + value
          }
        },
        getSelectionBarColor: function() {
          return '#27AE60';
        },
        getPointerColor: function() {
          return '#27AE60';
        }
      }
    };

    $scope.price_slider = {
      minValue: 0,
      maxValue: 2000,
      options: {
        floor: 0,
        ceil: 2000,
        translate: function(value, sliderId, label) {
          switch (label) {
            case 'model':
              return '$' + value;
            case 'high':
              return '$' + value;
            default:
              return '$' + value
          }
        },
        getSelectionBarColor: function() {
          return '#27AE60';
        },
        getPointerColor: function() {
          return '#27AE60';
        }
      }
    };

    $scope.time_slider = {
      minValue: 6,
      maxValue: 48,
      options: {
        floor: 6,
        ceil: 48,
        translate: function(value, sliderId, label) {
          switch (label) {
            case 'model':
              return '$' + value;
            case 'high':
              return '$' + value;
            default:
              return '$' + value
          }
        },
        getSelectionBarColor: function() {
          return '#27AE60';
        },
        getPointerColor: function() {
          return '#27AE60';
        }
      }
    };

    $scope.highway_slider = {
      minValue: 0,
      maxValue: 100,
      options: {
        floor: 0,
        ceil: 100,
        translate: function(value, sliderId, label) {
          switch (label) {
            case 'model':
              return '$' + value;
            case 'high':
              return '$' + value;
            default:
              return '$' + value
          }
        },
        getSelectionBarColor: function() {
          return '#27AE60';
        },
        getPointerColor: function() {
          return '#27AE60';
        }
      }
    };

    $scope.city_slider = {
      minValue: 0,
      maxValue: 100,
      options: {
        floor: 0,
        ceil: 100,
        translate: function(value, sliderId, label) {
          switch (label) {
            case 'model':
              return '$' + value;
            case 'high':
              return '$' + value;
            default:
              return '$' + value
          }
        },
        getSelectionBarColor: function() {
          return '#27AE60';
        },
        getPointerColor: function() {
          return '#27AE60';
        }
      }
    };

    $scope.combined_slider = {
      minValue: 0,
      maxValue: 100,
      options: {
        floor: 0,
        ceil: 100,
        translate: function(value, sliderId, label) {
          switch (label) {
            case 'model':
              return '$' + value;
            case 'high':
              return '$' + value;
            default:
              return '$' + value
          }
        },
        getSelectionBarColor: function() {
          return '#27AE60';
        },
        getPointerColor: function() {
          return '#27AE60';
        }
      }
    };

    $scope.openAll = function() {
      $scope.open_all_flag = true;
      $scope.price_slide_hide_flag = false;
      $scope.time_slide_hide_flag = false;
      $scope.car_model_hide_flag = false;
      $scope.warranty_hide_flag = false;
      $scope.car_type_hide_flag = false;
      $scope.driving_type_hide_flag = false;
      $scope.fuel_option_hide_flag = false;
      $scope.cylinders_hide_flag = false;
      $scope.transmistion_type_hide_flag = false;
      $scope.transmistion_gears_hide_flag = false;
    };

    $scope.closeAll = function() {
      $scope.open_all_flag = false;
      $scope.price_slide_hide_flag = true;
      $scope.time_slide_hide_flag = true;
      $scope.car_model_hide_flag = true;
      $scope.warranty_hide_flag = true;
      $scope.car_type_hide_flag = true;
      $scope.driving_type_hide_flag = true;
      $scope.fuel_option_hide_flag = true;
      $scope.cylinders_hide_flag = true;
      $scope.transmistion_type_hide_flag = true;
      $scope.transmistion_gears_hide_flag = true;
    };

    $scope.price_slide_hide = function() {
      $scope.price_slide_hide_flag = !$scope.price_slide_hide_flag;
    };

    $scope.time_slide_hide = function() {
      $scope.time_slide_hide_flag = !$scope.time_slide_hide_flag;
    };

    $scope.car_model_hide = function() {
      $scope.car_model_hide_flag = !$scope.car_model_hide_flag;
    };

    $scope.warranty_hide = function() {
      $scope.warranty_hide_flag = !$scope.warranty_hide_flag;
    };

    $scope.car_type_hide = function() {
      $scope.car_type_hide_flag = !$scope.car_type_hide_flag;
    };

    $scope.driving_type_hide = function() {
      $scope.driving_type_hide_flag = !$scope.driving_type_hide_flag;
    };

    $scope.fuel_option_hide = function() {
      $scope.fuel_option_hide_flag = !$scope.fuel_option_hide_flag;
    };

    $scope.cylinders_hide = function() {
      $scope.cylinders_hide_flag = !$scope.cylinders_hide_flag;
    };

    $scope.transmistion_type_hide = function() {
      $scope.transmistion_type_hide_flag = !$scope.transmistion_type_hide_flag;
    };

    $scope.transmistion_gears_hide = function() {
      $scope.transmistion_gears_hide_flag = !$scope.transmistion_gears_hide_flag;
    };

    $scope.moreCarModel = function() {
      if($scope.moreCarModel_flag) {
        $scope.car_model_count = 5;
      }
      else {
        $scope.car_model_count = $scope.car_model.length;
      }

      $scope.moreCarModel_flag = !$scope.moreCarModel_flag;
    };

    $scope.dropdownCarType = function() {
      console.log('dropdownCarType = ', $scope.showDropdownCarType);
      $scope.showDropdownCarType = !$scope.showDropdownCarType;
    };
  });

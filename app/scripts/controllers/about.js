'use strict';

/**
 * @ngdoc function
 * @name carfundApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the carfundApp
 */
angular.module('carfundApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

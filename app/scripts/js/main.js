$(document).ready(function() {
    // Sticky header function
    $(window).scroll(function() {
        if($(window).scrollTop() >= 66) {
            $('header').addClass('sticky');
            $('.selected-filters-box').addClass('sticky');
            $('.car-description-inner').addClass('sticky');
        } else {
            $('header').removeClass('sticky');
            $('.selected-filters-box').removeClass('sticky');
            $('.car-description-inner').removeClass('sticky');
        }
    });

    // Carousel settings
    $('.carousel').carousel({
	    pause: "false",
	    interval: 10000
	});


    // Left nav toggle scripts
    $('.left-navbar-toggle').click(function() {
        $('.page-wrap').addClass('fixed');
        $('.left-nav').addClass('open');
        $('body').addClass('nav-open');
    });

    $('.nav-overlay').click(function(){
        $('.page-wrap').removeClass('fixed');
        $('.left-nav').removeClass('open');
        $('body').removeClass('nav-open');
    });

    $('.left-nav .sign-in').click(function() {
        $('.left-nav .sign-up').removeClass('open');
        $('.left-nav .sign-up-container').slideUp('fast');
        $(this).toggleClass('open');
        $('.left-nav .sign-in-container').slideToggle('fast');
    });

    $('.left-nav .sign-up').click(function() {
        $('.left-nav .sign-in').removeClass('open');
        $('.left-nav .sign-in-container').slideUp('fast');
        $(this).toggleClass('open');
        $('.left-nav .sign-up-container').slideToggle('fast');
    });


    // Search outside click function
    $('body').click(function() {
        if($('.search-bar input').is(':focus')) {
            $('.search-bar .search-suggestions').slideDown('fast');
        } else {
            $('.search-bar .search-suggestions').slideUp('fast'); 
        }
    });

    // Mobile left navigatio toggle 
    $('.left-nav-toggle').click(function() {
        $('.left-filters').addClass('open');
    });
    $('.close-left-nav').click(function() {
        $('.left-filters').removeClass('open');
    });


    // Car list modal select all/none function
    $('#carListModal .model-items .item.select-all').click(function() {
        if($(this).hasClass('allSelected')) {
            $(this).find('p').html('Select All');
            $(this).removeClass("allSelected");
            $("#carListModal .model-items .item").removeClass('selected');
        } else {
            $(this).addClass("allSelected");
            $(this).find('p').html('Unselect All');
            $("#carListModal .model-items .item").addClass('selected');
        }
    });

    $('#carListModal .model-items .item').click(function() {
        $(this).toggleClass('selected');
    });
});


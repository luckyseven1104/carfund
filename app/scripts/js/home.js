$(document).ready(function() {
    $(".amount_slider").ionRangeSlider({
        type: "double",
        min: 0,
        max: 2000,
        step: 50,
        from: 450,
        to: 500,
        drag_interval: true
    });

    $('#price_select').select2({
        minimumResultsForSearch: Infinity
    });
    $('#month_select').select2({
        minimumResultsForSearch: Infinity
    });
});
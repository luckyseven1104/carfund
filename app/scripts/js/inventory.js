$(document).ready(function() {

    // Left filters functions
    $('.filter-title').click(function() {
        $(this).parent().find('.filter-items').slideToggle("fast");
        $(this).toggleClass('open');
    });

    $('.refine-all').click(function() {
        if ($(this).hasClass('open')) {
            $('.filter-items').slideUp('fast');
            $(this).html('Open All');
            $(this).removeClass('open');
            $(this).addClass('closed');
            $('.filter-title').addClass('open');
        } else {
            $('.filter-items').slideDown('fast');
            $(this).html('Close All');
            $(this).removeClass('closed');
            $(this).addClass('open');
            $('.filter-title').removeClass('open');
        }
    });

    $('.btn-more').click(function() {
        $(this).parent().find('.more-items').slideToggle('fast');
        $(this).toggleClass('open');
    });

    $(".amount_slider").ionRangeSlider({
        type: "double",
        min: 0,
        max: 2000,
        step: 50,
        from: 450,
        to: 500,
        drag_interval: true
    });

    $(".time_slider").ionRangeSlider({
        type: "double",
        min: 6,
        max: 48,
        step: 6,
        from: 12,
        to: 24,
        drag_interval: true
    });

    $(".MPG-city").ionRangeSlider({
        type: "double",
        min: 0,
        max: 100,
        step: 1,
        from: 40,
        to: 45,
        drag_interval: true
    });

    $(".MPG-combined").ionRangeSlider({
        type: "double",
        min: 0,
        max: 100,
        step: 1,
        from: 40,
        to: 45,
        drag_interval: true
    });

    $(".MPG-highway").ionRangeSlider({
        type: "double",
        min: 0,
        max: 100,
        step: 1,
        from: 40,
        to: 45,
        drag_interval: true
    });



    $('.car_brands').select2({
        placeholder: "BMW, Mercedes-Benz ..."
    });
    $('.car_brands_inner').select2();
    $('.page_limit_selector').select2({
        minimumResultsForSearch: Infinity
    });

});

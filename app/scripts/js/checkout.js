$(document).ready(function() {

	$('.options-inner .radio input[name=deliverOprions]').on('change', function() {
        $('.delivery-options').slideToggle("slow");
    });

    $(window).scroll(function() {
        if($(window).scrollTop() >= $('.checkout-inner').offset().top - 44) {
            $('.checkout-total').addClass('fixed');
        } else if ($(window).scrollTop() < $('.checkout-inner').offset().top - 44) {
            $('.checkout-total').removeClass('fixed');
        }
    });
});

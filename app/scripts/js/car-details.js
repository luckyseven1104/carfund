$(document).ready(function(){
    $('.car-slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.car-slider-nav'
    });

    $('.car-slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        padding: '20px',
        asNavFor: '.car-slider-for',
        centerMode: true,
        focusOnSelect: true,
        responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
                arrows: false,
                padding: '5px'
            }
        }]
    });
});